# redis

官网：https://redis.io/

概述

> Redis是什么？

Redis（Remote Dictionary Server )，即远程字典服务！

是一个开源的使用ANSI [C语言](https://baike.baidu.com/item/C语言)编写、支持网络、可基于内存亦可持久化的日志型、Key-Value[数据库](https://baike.baidu.com/item/数据库/103728)，并提供多种语言的API。

![image-20210123141256699](F:\javaStudy\Kuangsheng\redis\redis.assets\image-20210123141256699.png)



# redis入门

## linux安装

1.下载安装包 官网：https://redis.io/ （6.0 安装需自行升级gcc++）

2.解压Redis 的安装包！ 程序目录安放在`/opt`目录下

```bash
mv 目标文件 移动目录
#移动文件
tar -zxvf 目标文件
#解压文件
```

![image-20210124130917676](redis.assets/image-20210124130917676.png)

3.进入解压后的文件，可以看到我们redis的配置文件

![image-20210124131322261](redis.assets/image-20210124131322261.png)

4.基本的环境安装

```bash
yum install gcc-c++

make & make install
#（两次make） 
```

![image-20210124133513602](redis.assets/image-20210124133513602.png)

自行升级 gcc版本

5.redis的默认安装路径 `/usr/loacl/bin`

![image-20210124135040648](redis.assets/image-20210124135040648.png)

6.将redis 配置文件 `redis.conf` 复制到我们当前目录下

![image-20210124135150728](redis.assets/image-20210124135150728.png)

7.redis默认不是后台启动的，修改配置文件

![image-20210124135851724](redis.assets/image-20210124135851724.png)

`daemonize 改为yes`

 8.启动redis服务

```bash
pwd
#查看当前目录
redis-server kcongfig/redis.conf
redis-cli -p 6379
ping
keys *
#查看所有key
```

![image-20210124140922371](redis.assets/image-20210124140922371.png)

9.使用redis-cli进行连接测试！

![image-20210124141055882](redis.assets/image-20210124141055882.png)

10.Linux查看redis进程是否开启！

```bash
ps -ef|grep redis
#ps命令将某个进程显示出来 grep命令是查找  -e显示所有进程 -f全格式
```



![image-20210124141647636](redis.assets/image-20210124141647636.png)

11关闭redis服务 `shutdown`

![image-20210124142054171](redis.assets/image-20210124142054171.png)



12查看进程是否存在 `ps -ef|grep redis`

![image-20210124142644796](redis.assets/image-20210124142644796.png)



## 测试性能

**redis-benchmark**是一个压力测试工具

官方自带的性能测试工具

redis-benchmark命令参数！

| 序号 | 选项      | 描述                                       | 默认值    |
| :--- | :-------- | :----------------------------------------- | :-------- |
| 1    | **-h**    | 指定服务器主机名                           | 127.0.0.1 |
| 2    | **-p**    | 指定服务器端口                             | 6379      |
| 3    | **-s**    | 指定服务器 socket                          |           |
| 4    | **-c**    | 指定并发连接数                             | 50        |
| 5    | **-n**    | 指定请求数                                 | 10000     |
| 6    | **-d**    | 以字节的形式指定 SET/GET 值的数据大小      | 2         |
| 7    | **-k**    | 1=keep alive 0=reconnect                   | 1         |
| 8    | **-r**    | SET/GET/INCR 使用随机 key, SADD 使用随机值 |           |
| 9    | **-P**    | 通过管道传输 <numreq> 请求                 | 1         |
| 10   | **-q**    | 强制退出 redis。仅显示 query/sec 值        |           |
| 11   | **--csv** | 以 CSV 格式输出                            |           |
| 12   | **-l**    | 生成循环，永久执行测试                     |           |
| 13   | **-t**    | 仅运行以逗号分隔的测试命令列表。           |           |
| 14   | **-I**    | Idle 模式。仅打开 N 个 idle 连接并等待。   |           |

测试一

```bash
#测试：100个并发连接，10,0000请求
redis-benchamrk -h locakhost -p 6379 -c 100 -n 1000000
```

![image-20210124144820120](redis.assets/image-20210124144820120.png)

如何分析这些请求

比如set 100000requesr 100parallel 3bytes keep alive :1

![image-20210124144930631](redis.assets/image-20210124144930631.png)

## 基础的知识

redis默认有16个数据库

![image-20210124151327561](redis.assets/image-20210124151327561.png)

默认使用的是第0个数据库

可以使用select进行切换数据库！

```bash
127.0.0.1:6379> select 3 #切换数据库
OK
127.0.0.1:6379[3]> dbsize #查看DB大小
(integer) 0
```

![image-20210124152552707](redis.assets/image-20210124152552707.png)、、

```bash
127.0.0.1:6379[3]> keys * #查看数据库所有的key

```

清空当前数据库 `flushdb`

清空所有数据库的内容`flushall`

```bash
127.0.0.1:6379[3]> FLUSHDB
OK
127.0.0.1:6379[3]> keys *
(empty array)
127.0.0.1:6379[3]> 
```

> redis 是单线程的

明白redis是很快的，官方表示基于内存操作，cpu不是redis性能瓶颈，redis的瓶颈是根据机器的内存和网络带宽，既然可以使用单线程来使用，就使用单线程！

redis是C语言写的，官方提供的数据为100000+的qps

# 五大数据类型

> 官方文档

中文文档：http://www.redis.cn/

Redis 是一个开源（BSD许可）的，内存中的数据结构存储系统，它可以用作数据库、缓存和消息中间件。 它支持多种类型的数据结构，如 [字符串（strings）](http://www.redis.cn/topics/data-types-intro.html#strings)， [散列（hashes）](http://www.redis.cn/topics/data-types-intro.html#hashes)， [列表（lists）](http://www.redis.cn/topics/data-types-intro.html#lists)， [集合（sets）](http://www.redis.cn/topics/data-types-intro.html#sets)， [有序集合（sorted sets）](http://www.redis.cn/topics/data-types-intro.html#sorted-sets) 与范围查询， [bitmaps](http://www.redis.cn/topics/data-types-intro.html#bitmaps)， [hyperloglogs](http://www.redis.cn/topics/data-types-intro.html#hyperloglogs) 和 [地理空间（geospatial）](http://www.redis.cn/commands/geoadd.html) 索引半径查询。 Redis 内置了 [复制（replication）](http://www.redis.cn/topics/replication.html)，[LUA脚本（Lua scripting）](http://www.redis.cn/commands/eval.html)， [LRU驱动事件（LRU eviction）](http://www.redis.cn/topics/lru-cache.html)，[事务（transactions）](http://www.redis.cn/topics/transactions.html) 和不同级别的 [磁盘持久化（persistence）](http://www.redis.cn/topics/persistence.html)， 并通过 [Redis哨兵（Sentinel）](http://www.redis.cn/topics/sentinel.html)和自动 [分区（Cluster）](http://www.redis.cn/topics/cluster-tutorial.html)提供高可用性（high availability）。



## Redis-key

>重要 会涉及单点登录



```bash
127.0.0.1:6379> set name kuangshen #set key
OK
127.0.0.1:6379> set age 1 #set key
OK
127.0.0.1:6379> keys * #查看所有key
1) "age"
2) "name"
127.0.0.1:6379> exists name #判断key是否存在 存在返回1  不存在返回0
(integer) 1
127.0.0.1:6379> exists name1
(integer) 0
127.0.0.1:6379> move name 1 #移除 key
(integer) 1
127.0.0.1:6379> keys * #查看key
1) "age"
127.0.0.1:6379> set name kuang
OK
127.0.0.1:6379> keys *
1) "name"
2) "age"
127.0.0.1:6379> get name
"kuang"
127.0.0.1:6379>  expire name 10 #设置key的过期时间 ，单位是秒
(integer) 1
127.0.0.1:6379> ttl name #查看当前key的剩余时间
(integer) 3
127.0.0.1:6379> ttl name
(integer) 1
127.0.0.1:6379> ttl name
(integer) -2
127.0.0.1:6379> get name
(nil)
127.0.0.1:6379> set name ks #查看当前key的类型！
OK
127.0.0.1:6379> type name
string

```



## String(字符串)

90%的java程序员使用redis只会一个String类型

```bash
###############################################################
127.0.0.1:6379> set key1 v1  #设置值
OK
127.0.0.1:6379> get key1    #获得值
"v1"
127.0.0.1:6379> keys *     #获得所有值
1) "key1"
2) "name"
3) "age"
127.0.0.1:6379>  exists key1  # 判断是否存在这个key
(integer) 1
127.0.0.1:6379> APPEND key1 "hello"  # 追加字符串，如果当前key不存在，就相当于set key
(integer) 7
127.0.0.1:6379> get key1
"v1hello"
127.0.0.1:6379> strlen key1   # 获取字符串长度
(integer) 7
127.0.0.1:6379> append key1 ",kuangshen"
(integer) 17
127.0.0.1:6379> strlen key1
(integer) 17
127.0.0.1:6379> 
#########################################################
127.0.0.1:6379> set views 0  #初始浏览量为0
OK
127.0.0.1:6379> get views
"0"
127.0.0.1:6379> incr views #i++ 自增1 浏览量为1
(integer) 1
127.0.0.1:6379> incr views
(integer) 2
127.0.0.1:6379> get views
"2"
127.0.0.1:6379> decr views #i-- 自减1 浏览量-1
(integer) 1
127.0.0.1:6379> decr views
(integer) 0
127.0.0.1:6379> decr views
(integer) -1
127.0.0.1:6379> get views
"-1"
127.0.0.1:6379> incrby views 10   #设置步长 ，指定增量
(integer) 9
127.0.0.1:6379> incrby views 10
(integer) 19
127.0.0.1:6379> get views
"19"
127.0.0.1:6379> decrby views 5  #设置步长 ，指定减量
(integer) 14
127.0.0.1:6379> get views
"14"
#############################################################
#字符串范围 range
127.0.0.1:6379> set key1 "hello,kuangshen" #设置key1的值
OK
127.0.0.1:6379> get key1
"hello,kuangshen"
127.0.0.1:6379> getrange key1 0 3  #截取字符串[0,3]
"hell"
127.0.0.1:6379> getrange key1 0 -1  #获取全部的字符串，和get key 一样
"hello,kuangshen"
#字符串 替换replace
127.0.0.1:6379> set key2 abcdefg
OK
127.0.0.1:6379> get key2
"abcdefg"
127.0.0.1:6379> setrange key2 0 xxx  #替换指定位置开始的字符串
(integer) 7
127.0.0.1:6379> get key2
"xxxdefg"
#############################################################
#setex (set with expire) #设置过期时间
#setnx(see if not exist) #不存在的时候 设置生效
127.0.0.1:6379> setex key3 30 "hello"  #设置key3的值为hello 30秒后过期
OK
127.0.0.1:6379> ttl key3
(integer) 24
127.0.0.1:6379> get key3
"hello"
127.0.0.1:6379> setnx mykey "redis" #如果mykey不存在，创建mykey
(integer) 1
127.0.0.1:6379> keys *
1) "key1"
2) "key2"
3) "mykey"
127.0.0.1:6379> ttl key3
(integer) -2
127.0.0.1:6379> setnx mykey "MongoDb" #如果mykey存在，创建mykey失败
(integer) 0
127.0.0.1:6379> get mykey
"redis"
#############################################################
# 批量操作 mset mget
127.0.0.1:6379> mset k1 v1 k2 v2 k3 v3  #同时设置多个值
OK
127.0.0.1:6379> keys *
1) "k3"
2) "k1"
3) "k2"
127.0.0.1:6379> mget k1 k2 k3 #同时获取多个值
1) "v1"
2) "v2"
3) "v3"
127.0.0.1:6379> msetnx k1 v1 k4 v4  #msetnx（setnx） 是一个原子性的操作，要么一起成功，要么一起失败
(integer) 0
#########################
#对象 
#这里的key是一个巧妙的社交 set user：1{user:1:name}：{zhangsan}，{user:1:age}：{2}
127.0.0.1:6379> mset user:1:name zhangsan user:1:age 2
OK
127.0.0.1:6379> mget user:1:name user:1:age
1) "zhangsan"
2) "2"
########################################################
#get set 先get后set
  127.0.0.1:6379> getset db redis #如果不存在，则返回nil 设置值
(nil)
127.0.0.1:6379> getset db mongodb  #如果存在，获取原来的值，并设置新的值
"redis"
127.0.0.1:6379> getset db a
"mongodb"

```

数据结构是相同的！

String类似的使用场景：valu除了是我们的字符串还可以是我们的数字

* 计数器
* 统计多单位的数量 uid：95256449：follow：0 incr
* 粉丝数
* 对象缓存存储！



## List（列表）

基本的数据类型，列表

![image-20210124211425232](redis.assets/image-20210124211425232.png)

在redis里面，我们可以把list玩成，栈，队列，堵塞队列！

所有的list命令都是用l开头的！redis不区分大小写命令 ，list的值可重复

```bash
#######################################
127.0.0.1:6379> lpush list one #将一个值或者多个值，插入到列表头部（左）
(integer) 1
127.0.0.1:6379> lpush list two
(integer) 2
127.0.0.1:6379> lpush list three
(integer) 3
127.0.0.1:6379> LRANGE list 0 -1 #获取list的值
1) "three"
2) "two"
3) "one"
127.0.0.1:6379> LRANGE list 0 1
1) "three"
2) "two"
127.0.0.1:6379> Rpush list righr #将一个值或者多个值，插入到列表尾部（右）
(integer) 4
127.0.0.1:6379> LRANGE list 0 1
1) "three"
2) "two"
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "two"
3) "one"
4) "righr"
######################################
#lpop rpop

127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "two"
3) "one"
4) "righr"
127.0.0.1:6379> lpop list  #移除list的第一个元素
"three"
127.0.0.1:6379> rpop list  #移除list的最后一个元素
"righr"
127.0.0.1:6379> LRANGE list 0 -1
1) "two"
2) "one"
######################################
127.0.0.1:6379> LRANGE list 0 -1
1) "two"
2) "one"
127.0.0.1:6379>  lindex list 1  #获得下标为1的值
"one"
127.0.0.1:6379>  lindex list 0  #获得下标为0的值
"two"
#########################
#Llen
127.0.0.1:6379> lpush list one
(integer) 1
127.0.0.1:6379> lpush list two
(integer) 2
127.0.0.1:6379> lpush list three
(integer) 3
127.0.0.1:6379> llen list #返回列表的长度
(integer) 3
################################
#lrem 移除
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
4) "one"
127.0.0.1:6379> lrem list 1 one #移除list集合中指定个数的value，精确匹配
(integer) 1
127.0.0.1:6379> LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
127.0.0.1:6379> lrem list 1 three
(integer) 1
127.0.0.1:6379> lpush list three
(integer) 3
127.0.0.1:6379> lrem list 2 three
(integer) 2
127.0.0.1:6379> LRANGE list 0 -1
1) "two"
################################
#trim 修剪 list 截断
127.0.0.1:6379> Rpush mylist "hello"
(integer) 1
127.0.0.1:6379> Rpush mylist "hello1"
(integer) 2
127.0.0.1:6379> Rpush mylist "hello2:
Invalid argument(s)
127.0.0.1:6379> Rpush mylist "hello2"
(integer) 3
127.0.0.1:6379> Rpush mylist "hello3"
(integer) 4
127.0.0.1:6379> LRANGE mylist 0 -1
1) "hello"
2) "hello1"
3) "hello2"
4) "hello3"
127.0.0.1:6379> ltrim mylist 1 2  #通过下标截取指定的长度，这个list已经被改变了，截断了只剩下截取的元素
OK
127.0.0.1:6379> LRANGE mylist 0 -1
1) "hello1"
2) "hello2"
###########################################
#rpoplpush 移除列表的最后一个元素，将它移到新的列表中！
127.0.0.1:6379> rpush mylist "hello"
(integer) 1
127.0.0.1:6379> rpush mylist "hello1"
(integer) 2
127.0.0.1:6379> rpush mylist "hello2"
(integer) 3
127.0.0.1:6379> rpoplpush mylist myotherlist # 移除列表的最后一个元素，将它移到新的列表中！
"hello2"
127.0.0.1:6379> LRANGE mylist 0 -1 #查看原来的列表
1) "hello"
2) "hello1"
127.0.0.1:6379> LRANGE myotherlist 0 -1 #查看目标列表 ，确实存在改变
1) "hello2"
#############################################
#lset 将列表中指定下标替换为另一个值，更新操作
#exists 判断是否存在 
127.0.0.1:6379> exists list #判断是否存在 
(integer) 0
127.0.0.1:6379> lset list 0 iotem #如果不存在列表 我们去更新会报错
(error) ERR no such key
127.0.0.1:6379> lpush list value
(integer) 1
127.0.0.1:6379> LRANGE list 0 0
1) "value"
127.0.0.1:6379> lset list 0 item #如果存在列表 更新下标的值
OK
127.0.0.1:6379> LRANGE list 0 0
1) "item"
127.0.0.1:6379> lset list 1 other  #如果不存在下标，会报错
(error) ERR index out of range
#############################################
#linsert 将具体的某个值value插入到某个元素的前面或后面
127.0.0.1:6379> rpush mylist "hello"
(integer) 1
127.0.0.1:6379> rpush mylist "world"
(integer) 2
127.0.0.1:6379> LINSERT mylist before "world" "other" #插入在world的前面
(integer) 3
127.0.0.1:6379> LRANGE mylist 0 -1
1) "hello"
2) "other"
3) "world"
127.0.0.1:6379> LINSERT mylist after world new #插入在new的后面
(integer) 4
127.0.0.1:6379> LRANGE mylist 0 -1
1) "hello"
2) "other"
3) "world"
4) "new"



```

> 小结

- 它实际上是一个链表，before node after ，left， right都可以插入值
- 如果key不存在，创建新的链表
- 如果key存在，新增内容
- 如果移除了所有值，空链表，也代表不存在
- 在两边插入或者改动值，效率最高，中间元素，相对来说效率会低一点

消息队列！（lpush ，rpop） 栈（Lpush Lpop）



## Set（集合）

set中的值是不能重复读的！

s开头

```bash
##################################
127.0.0.1:6379> sadd myset "hello" #set中添加值
(integer) 1
127.0.0.1:6379> sadd myset "kuangshen"
(integer) 1
127.0.0.1:6379> sadd myset "lovekuangshen"
(integer) 1
127.0.0.1:6379> SMEMBERS myset   #查看set的所有值
1) "hello"
2) "lovekuangshen"
3) "kuangshen"
127.0.0.1:6379> SISMEMBER myset hello  #判断某个值是不是在set集合中！ 是返回1 
(integer) 1
127.0.0.1:6379> SISMEMBER myset world
(integer) 0
##########################################
#scard 获取元素个数
127.0.0.1:6379> scard myset #获取元素个数
(integer) 3
127.0.0.1:6379> sadd myset kuangshen #set 不允许重复
(integer) 0
127.0.0.1:6379> sadd myset kuangshen1
(integer) 1
127.0.0.1:6379> scard myset
(integer) 4
#######################################
#rem
127.0.0.1:6379> srem myset hello #移除set中的指定元素
(integer) 1
127.0.0.1:6379> scard myset
(integer) 3
127.0.0.1:6379> SMEMBERS myset
1) "kuangshen1"
2) "lovekuangshen"
3) "kuangshen"
########################################
#set无序不重复，抽随机
127.0.0.1:6379> SMEMBERS myset
1) "kuangshen1"
2) "lovekuangshen"
3) "kuangshen"
127.0.0.1:6379> SRANDMEMBER myset #随机抽取一个元素
"kuangshen"
127.0.0.1:6379> SRANDMEMBER myset  #随机抽取一个元素
"lovekuangshen"
127.0.0.1:6379> SRANDMEMBER myset 2 #随机抽取指定元素个数
1) "kuangshen1"
2) "lovekuangshen"
127.0.0.1:6379> SRANDMEMBER myset 2 #随机抽取指定元素个数
1) "lovekuangshen"
2) "kuangshen"
#################################################
#移除 spop
127.0.0.1:6379> SMEMBERS myset
1) "kuangshen1"
2) "lovekuangshen"
3) "kuangshen"
127.0.0.1:6379> spop myset #随机删除set集合中的元素
"kuangshen"
127.0.0.1:6379> spop myset #随机删除set集合中的元素
"lovekuangshen"
127.0.0.1:6379> SMEMBERS myset
1) "kuangshen1"
##############################################
#smove 移动 将一个值移动到另一个set集合中
127.0.0.1:6379> sadd myset hello
(integer) 1
127.0.0.1:6379> sadd myset world
(integer) 1
127.0.0.1:6379> sadd myset kuangshen
(integer) 1
127.0.0.1:6379> sadd myset2 set2
(integer) 1
127.0.0.1:6379> smove myset myset2 kuangshen #将一个值移动到另一个set集合中
(integer) 1
127.0.0.1:6379> SMEMBERS myset
1) "hello"
2) "world"
127.0.0.1:6379> SMEMBERS myset2
1) "set2"
2) "kuangshen"
################################################
微博 ，B站共同关注！（并集）
数字集合类
- 差集
- 交集
- 并集
127.0.0.1:6379> sadd key1 a
(integer) 1
127.0.0.1:6379> sadd key1 b
(integer) 1
127.0.0.1:6379> sadd key1 c
(integer) 1
127.0.0.1:6379> sadd key2 c
(integer) 1
127.0.0.1:6379> sadd key2 d
(integer) 1
127.0.0.1:6379> sadd key2 e
(integer) 1
127.0.0.1:6379> sdiff key1 key2 #差集
1) "b"
2) "a"
127.0.0.1:6379> sdiff key2 key1 #差集
1) "d"
2) "e"
127.0.0.1:6379> sinter key1 key2  #交集 共同好友
1) "c" 
127.0.0.1:6379> sunion key1 key2  #并集
1) "a"
2) "d"
3) "c"
4) "b"
5) "e"

```

微博，A用户将所有关注的人放在一个set集合中，将它的粉丝也放在一个集合中！

共同关注，共同爱好，二度好友，推荐好友！（六度分割理论）









## Hash（哈希）

Map集合，即key-map 时候这个值就是一个map集合 本质和String类型没有太大的区别，还是一个简单的key-value

set myhash  field kuangshen

```bash
#####################################
127.0.0.1:6379> hset myhash field1 kuangshen #set 一个具体的key-value
(integer) 1
127.0.0.1:6379> hget myhash field1 ##获取一个字段值
"kuangshen"
127.0.0.1:6379> hmset myhash field1 hello field2 world  #set 多个具体的key-value
OK
127.0.0.1:6379> hmget myhash field1 field2 #获取多个字段值
1) "hello"
2) "world"
127.0.0.1:6379> hgetall myhash #获取全部字段的数据
1) "field1"
2) "hello"
3) "field2"
4) "world"
127.0.0.1:6379> hdel myhash field1  #删除指定hash指定字段，对应的value也就没有了
(integer) 1
127.0.0.1:6379> hgetall myhash
1) "field2"
2) "world"
###############################################
#hlen #获取hash表的字段数量！
127.0.0.1:6379> hmset myhash field1 hello field2 world
OK
127.0.0.1:6379> HGETALL myhash
1) "field2"
2) "world"
3) "field1"
4) "hello"
127.0.0.1:6379> hlen myhash #获取hash表的字段数量！
(integer) 2
#######################################
127.0.0.1:6379> HEXISTS myhash field1 #判断hash中指定字段是否存在
(integer) 1
127.0.0.1:6379> HEXISTS myhash field
(integer) 0
#####################################
#只获得所有的key
#只获得所有的value
127.0.0.1:6379> hkeys myhash #只获得所有的field
1) "field2"
2) "field1"
127.0.0.1:6379> hvals myhash #只获得所有的value
1) "world"
2) "hello"
##################################
#incr decr 
127.0.0.1:6379> hset myhash field3 5 #指定增量 5
(integer) 1
127.0.0.1:6379> HINCRBY myhash field3 1 #增量 1
(integer) 6
127.0.0.1:6379> HINCRBY myhash field3 -1
(integer) 5
127.0.0.1:6379> hsetnx myhash field4 hello #如果不存在则可以设置
(integer) 1
127.0.0.1:6379> hsetnx myhash field4 world #如果存在则不可以设置
(integer) 0

```

hash变更的数据user name age 尤其是用户信息之类的，经常变动的信息！

`hash更适合于对象的存储，String更加适合字符串的存储`





 

## Zset（有序集合）

在set的基础上，增加了一个值，set k1 v1     `zset k1 score1（float）  v1`

```bash
###################################
127.0.0.1:6379> zadd myset 1 one #添加一个值
(integer) 1
127.0.0.1:6379> zadd myset 2 two 3 three  #添加多个值
(integer) 2
127.0.0.1:6379> ZRANGE myset 0 -1
1) "one"
2) "two"
3) "three"
################################
排序如何实现
#ZRANGEBYSCORE key min max
127.0.0.1:6379> zadd salary 2500 xiaohong
(integer) 1
127.0.0.1:6379> zadd salary 5000 kuangshen
(integer) 1
127.0.0.1:6379> zadd salary zhansan 500
(error) ERR value is not a valid float
127.0.0.1:6379> zadd salary 500 zhansan
(integer) 1
127.0.0.1:6379> ZRANGEBYSCORE salary -inf +inf #显示全部的用户 从小到大！
1) "zhansan"
2) "xiaohong"
3) "kuangshen"
127.0.0.1:6379> ZREVRANGE salary  0 -1  #从大到小 ZREVRANGE
1) "kuangshen"
2) "zhansan"
127.0.0.1:6379> ZRANGEBYSCORE salary -inf +inf withscores #显示全部的用户附加成绩 从小到大！
1) "zhansan"
2) "500"
3) "xiaohong"
4) "2500"
5) "kuangshen"
6) "5000"
127.0.0.1:6379> ZRANGEBYSCORE salary -inf 2500 withscores #显示全部的用户附加成绩 从小到大 最大2500！
1) "zhansan"
2) "500"
3) "xiaohong"
4) "2500"
############################################
#zrem 移除
127.0.0.1:6379> ZRANGE salary 0 -1
1) "zhansan"
2) "xiaohong"
3) "kuangshen"
127.0.0.1:6379> zrem salary xiaohong #精确移除 移除有序集合中的指定元素
(integer) 1
127.0.0.1:6379> ZRANGE salary 0 -1
1) "zhansan"
2) "kuangshen"
127.0.0.1:6379> ZCARD salary  #zcard 获取长度
(integer) 2
##################################################
#zcount
127.0.0.1:6379> zadd myset 1 hello
(integer) 1
127.0.0.1:6379> zadd myset 2 world 3 kuangshen
(integer) 2
127.0.0.1:6379> ZCOUNT myset 1 3 #获取指定区间的成员数量
(integer) 3
127.0.0.1:6379> ZCOUNT myset 1 2
(integer) 2

```

其余的一些api，通过学习，可以去查官网文档

案例思路：set 排序 ，存储班级成绩 ，工资表排序

普通消息，1，重要消息2，带权重进行判断！

排行榜应用实现，取Top N测试





# 三种特殊数据类型

## geospat（地理位置）

朋友的定位，附件的人，打车距离计算？

redis的Geo在redis3.2版本就推出了！这个功能可以推算出地理位置的信息，两地之间的距离，方圆几里的人！

可以查询一些测试数据 http://www.hao828.com/chaxun/zhongguochengshijingweidu/ 城市地理位置

只有6个命令

![image-20210126130705657](redis.assets/image-20210126130705657.png)

> geoAdd

```bash
#########################
#geoadd 添加地理位置
#规则：两级无法直接添加，我们一般会下载城市数据，直接通过java程序一次性导入
#参数 key 值
# 有效的经度从-180度到180度。 
# 有效的纬度从-85.05112878度到85.05112878度。
# 当坐标位置超出上述指定范围时，该命令将会返回一个错误。
#127.0.0.1:6379> GEOADD china:city 39.90 116.40 beijing
#(error) ERR invalid longitude,latitude pair 39.900000,116.400000
127.0.0.1:6379> GEOADD china:city 116.4 39.9 beijing
(integer) 1
127.0.0.1:6379> GEOADD china:city 121.47 31.23 shanghai
(integer) 1
127.0.0.1:6379> GEOADD china:city 106.5 29.53 chongqi 114.05 22.52 shenzhen
(integer) 2
127.0.0.1:6379> GEOADD china:city 120.16 30.24 hangzhou 108.96 34.26 xian
(integer) 2

```

> geops

获取

```bash
###################
127.0.0.1:6379> GEOPOS china:city beijing  # 获取指定的城市的经度和纬度！
1) 1) "116.39999896287918091"
   2) "39.90000009167092543"

```

> geodist 两人之间的距离

```bash
单位：
 - m 表示单位为米。 
 - km 表示单位为千米。
 - mi 表示单位为英里。 
 - ft 表示单位为英尺。
 127.0.0.1:6379> GEODIST china:city beijing shanghai km  ## 查看上海到北京的直线距离 
"1067.3788"

```

> georadius 以给定的经纬度为中心， 找出某一半径内的元素

适用于找朋友

```bash
附近的人？ （获得所有附近的人的地址，定位！）通过半径来查询！
127.0.0.1:6379> GEORADIUS china:city 110 30 1000 km   # 以110，30 这个经纬度为中心，寻 找方圆1000km内的城市 
1) "chongqi"
2) "xian"
3) "shenzhen"
4) "hangzhou"
127.0.0.1:6379> GEORADIUS china:city 110 30 500 km withdist  # 显示到中间距离的位置 500以内的城市
1) 1) "chongqi"
   2) "341.9374"
2) 1) "xian"
   2) "483.8340"
127.0.0.1:6379> GEORADIUS china:city 110 30 1000 km withcoord   # 显示他人的定位信息
1) 1) "chongqi"
   2) 1) "106.49999767541885376"
      2) "29.52999957900659211"
2) 1) "xian"
   2) 1) "108.96000176668167114"
      2) "34.25999964418929977"
3) 1) "shenzhen"
   2) 1) "114.04999762773513794"
      2) "22.5200000879503861"
4) 1) "hangzhou"
   2) 1) "120.1600000262260437"
      2) "30.2400003229490224"
127.0.0.1:6379> GEORADIUS china:city 110 30 1000 km withdist withcoord count 1  # 筛选出指定的结果！
1) 1) "chongqi"
   2) "341.9374"
   3) 1) "106.49999767541885376"
      2) "29.52999957900659211"

```

> 适用于地图定位 找周围的城市 GEORADIUSBYMEMBER 

```bash
#找出位于指定元素周围的其他元素！
127.0.0.1:6379> GEORADIUSBYMEMBER china:city beijing 1000 km
1) "beijing"
2) "xian"
127.0.0.1:6379> GEORADIUSBYMEMBER china:city shanghai 400 km
1) "hangzhou"
2) "shanghai"
```

> geohash命令 -返回一个或者多个位置的geohash表示

```bash
#该命令将返回11个字符的geohash字符串
# 将二维的经纬度转换为一维的字符串，如果两个字符串越接近，那么则距离越近！
127.0.0.1:6379> geohash china:city beijing chongqi
1) "wx4fbxxfke0"
2) "wm5xzrybty0"

```

>zset geo

```bash
GEO 底层的实现原理其实就是 Zset！我们可以使用Zset命令来操作geo！

ZRANGE china:city 0 -1  # 查看地图中全部的元素 
1) "chongqi"
2) "xian"
3) "shenzhen"
4) "hangzhou"
5) "shanghai"
6) "beijing"
zrem china:city beijing  # 移除指定元素！ 
127.0.0.1:6379> ZRANGE china:city 0 -1
1) "chongqi"
2) "xian"
3) "shenzhen"
4) "hangzhou"
5) "shanghai"

```



## hyperloglog（基数统计）

> 什么是基数

A{1,3.5,7,8,7}

B{1,3,5,7,8}

基数（不重复的元素）=5个，可以接受误差

> 简介

redis 2.8.9 版本就更新了hyperloglog数据结构

redis hyperloglog 基数统计的算法

优点：占用的内存是固定，2^64不同的元素的技术，只需要废12kb，如果要从内存角度来比较的话 hyperloglog首选

**网页的UV** （一个人访问网站多次 但是还是算作一个人）

传统的方式，set保存用户的id，然后就可以统计set中的元素数量作为标准判断！

这个方式如果保存大量的用户id 很麻烦 我们的目的是为了计数 ，而不是保存用户id

0.81%错误率 统计UV任务 可以忽略不计

> 测试使用

```bash
127.0.0.1:6379> pfadd mykey a b c d e f g h i j #创建第一组元素 mykey 
(integer) 1
127.0.0.1:6379> PFCOUNT mykey #统计第一组元素 mykey 
(integer) 10
127.0.0.1:6379> pfadd mykey2  i j z x c v b n m  #创建第二组元素 mykey2 
(integer) 1
127.0.0.1:6379> PFCOUNT mykey2  #统计第二组元素 mykey 
(integer) 9
127.0.0.1:6379> PFMERGE mykey3 mykey mykey2  #合并二组元素 mykey mykey2 =》mykey3
OK
127.0.0.1:6379> PFCOUNT mykey3 #统计第三组元素 
(integer) 15

```

如果允许容错，那么一定可以使用hyperloglog

如果不允许容错，就使用set或者自己的数据类型即可





## bitmaps（位图场景）

> 位存储

统计用户信息，活跃，不活跃，`登录、未登录`！打卡，365打卡！两个状态的，都可以使用bitmap

Bitmaps位图 数据结构 都是操作二进制位来进行记录，就只有`0和1`两个状态

365天=365bit 1字节=8bit 46字节左右



> 测试

使用bitmap来记录周一到周日的打卡状态

周一：1 周二：0 周三：1 周四：0 …

![image-20210126150541338](redis.assets/image-20210126150541338.png)

```bash
127.0.0.1:6379> setbit sign 0 1
(integer) 0
127.0.0.1:6379> setbit sign 1 0
(integer) 0
127.0.0.1:6379> setbit sign 2 0
(integer) 0
127.0.0.1:6379> setbit sign 3 1
(integer) 0
127.0.0.1:6379> setbit sign 4 1
(integer) 0
127.0.0.1:6379> setbit sign 5 0
(integer) 0
127.0.0.1:6379> setbit sign 6 0
(integer) 0
127.0.0.1:6379> getbit sign 3 #查看是否打卡
(integer) 1
127.0.0.1:6379> getbit sign 6
(integer) 0
127.0.0.1:6379> BITCOUNT sign #统计这周打卡记录 是否全勤
(integer) 3


```











# 事务

事务的本质：一切命令的集合！一个事务中的所有命令都会被序列化，在事务执行过程中，会按照顺序来执行！

一次性、顺序性、排他性、执行一些列的命令！

redis事务没有隔离级别的概念

所有命令在事务中，并没有直接被执行，只有发起执行命令的时候才被执行！EXec

==Redis单条命令式保存原子性，但是事务不保证原子性==

redis的事务：

* 开启事务（multi）
* 命令入队（...）
* 执行事务（exec）

> 正常执行事务

`一组事务从multi 到exec`

```bash
127.0.0.1:6379> multi   #开启事务
OK
127.0.0.1:6379> set k1 vi
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> get k2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED 
127.0.0.1:6379> exec  #执行事务
1) OK
2) OK
3) "v2"
4) OK

```

> 放弃事务



```bash
127.0.0.1:6379> multi #开启事务
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k4 v4
QUEUED
127.0.0.1:6379> discard  #取消事务
OK
127.0.0.1:6379> get k4  #事务队列中的命令不会去执行
(nil)

```



> 编译型异常（代码有问题！命令有错），事务中所有命令都不会被执行

```bash
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> getset k3 #错误的命令
(error) ERR wrong number of arguments for 'getset' command
127.0.0.1:6379> set k4 v4
QUEUED
127.0.0.1:6379> set k5 v5
QUEUED
127.0.0.1:6379> exec  #执行报错
(error) EXECABORT Transaction discarded because of previous errors.
127.0.0.1:6379> get  k5 #所有的命令都不会被执行
(nil)

```



> 运行时异常（1/0），如果事务队列中存在语法性，那么执行命令的时候，其他命令是可以正常执行的，错误命令抛出异常！

```bash
127.0.0.1:6379> set k1 v1
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379> incr k1 #会执行失败
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> get k3
QUEUED
127.0.0.1:6379> exec  #虽然第一条执行失败报错了，但是依旧能够正常执行
1) (error) ERR value is not an integer or out of range
2) OK
3) OK
4) "v3"
127.0.0.1:6379> get k2
"v2"
127.0.0.1:6379> get k3
"v3"

```

> 监控 watch

**悲观锁**

* 很悲观，认为什么时候都会出现问题，无论做什么都会加锁

乐观锁

- 很乐观 认为什么时候都不会出现问题，所以不会上锁！更新数据的时候去判断一下，在此期间是否有人修改过这个数据
- 获取version
- 更新时比较version

> redis测监控

 正常

```bash
127.0.0.1:6379> set money 100
OK
127.0.0.1:6379> set out 0
OK
127.0.0.1:6379> watch money  #监视money
OK
127.0.0.1:6379> multi #事务正常结束，数据期间没有发生变动，这个时候就正常执行成功！
OK
127.0.0.1:6379> DECRBY money 20
QUEUED
127.0.0.1:6379> DECRBY out 20
QUEUED
127.0.0.1:6379> exec
1) (integer) 80
2) (integer) -20

```

测试多线程修改值 使用watch 可以当作redis 的乐观锁操作！

```bash
127.0.0.1:6379> watch money  # 监视  money
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379> decrby money by
QUEUED
127.0.0.1:6379> INCRBY out 10
QUEUED
127.0.0.1:6379> exec  # 执行之前，另外一个线程，修改了我们的值，这个时候，就会导致事务执行失败！
(nil)
127.0.0.1:6379> 

另一个线程
127.0.0.1:6379> get money
"80"
127.0.0.1:6379> decrby money 10
(integer) 70

```



```bash
127.0.0.1:6379> unwatch   #/发现事务执行失败，就先解锁，取消监视
OK
127.0.0.1:6379> watch money  #获取最新的值，再次监视，获取新版本
OK
127.0.0.1:6379> multi
OK
127.0.0.1:6379> DECRBY money 1
QUEUED
127.0.0.1:6379> incrby out 1
QUEUED
127.0.0.1:6379> exec   #比对监视的值是否发生变化，如果没有发生变化，那么可以执行成功，如果变化了就执行失败
1) (integer) 69
2) (integer) -19

```

![image-20210126170505958](redis.assets/image-20210126170505958.png)





# jedis

我们要使用java来操作redis

> 什么是jedis 是官方推荐的java连接工具！使用java操作redis中间件！如果你要使用java操作redis，那么对jedis一定是非常熟悉的

知其然并致其所然，授之以渔

> 测试

1.导入对应的依赖

```xml
        <dependency>
            <groupId>redis.clients</groupId>
            <artifactId>jedis</artifactId>
            <version>3.2.0</version>
        </dependency>
<!--        fastjson-->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.62</version>
        </dependency>
```

2.编码测试

* 连接数据库
* 操作命令
* 断开连接

```java
public static void main(String[] args) {
    Jedis jedis=new Jedis("127.0.0.1",6379);
    System.out.println(jedis.ping());
}
//输出
PONG
```

## 常用的api

String

List

Set

Hash

Zset

> 所有api都是上面的指令



> 事务

```java
public static void main(String[] args) {
    Jedis jedis=new Jedis("127.0.0.1",6379);
    JSONObject jsonObject=new JSONObject();
    jsonObject.put("hello","world");
    jsonObject.put("name","kuangshen");
    jedis.flushDB();
    //开启事务
    Transaction multi=jedis.multi();
    String result = jsonObject.toJSONString();

    try {
        multi.set("user1",result);
        multi.set("user2",result);
        int i=1/0;//代码出现错误
        multi.exec(); //执行事务
    } catch (Exception e) {
        e.printStackTrace();
    }finally {
        System.out.println(jedis.get("user1"));
        System.out.println(jedis.get("user2"));
        jedis.close();
    }

}
```



















# springboot整合

springboot操作数据库：springboot jpa jdbc mongodb redis

springData也是和springboot齐名的项目

说明：在springboot2.x之后，原来的jedis被替换成了lettuce

jedis：采用的直连，多个线程操作的话，是不安全的，如果想要避免不安全，使用jedis pool连接池！更像BIO模式

lettuce：采用netty，实例可以再多个线程中进行共享，不存在线程不安全的情况！可以减少线程数据了，更像NIO模式

源码分析

```java
public class RedisAutoConfiguration {

   @Bean
   @ConditionalOnMissingBean(name = "redisTemplate")//我们定义一个redisTemplate 来替换这个默认的
   public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory)
         throws UnknownHostException {
       //默认的redisTemplate没有过多的设置 redis对象需要序列化！
       //两个泛型都是Object Object的类型 我们需要转换为 String Object
      RedisTemplate<Object, Object> template = new RedisTemplate<>();
      template.setConnectionFactory(redisConnectionFactory);
      return template;
   }

   @Bean
   @ConditionalOnMissingBean //由于String是redis 中最常用的类型，所以单独提出来了一个Bean
   public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory)
         throws UnknownHostException {
      StringRedisTemplate template = new StringRedisTemplate();
      template.setConnectionFactory(redisConnectionFactory);
      return template;
   }

}
```

>整合测试

1.导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

2.配置连接

```properties
spring.redis.host=127.0.0.1
spring.redis.port=6379
```

3.测试

```java
@SpringBootTest
class DemoApplicationTests {
    @Autowired
     private RedisTemplate redisTemplate;

    @Test
    void contextLoads() {
        //RedisTemplate 操作不同的类型
        //opsForValue 操作字符串
        //opsForList list
        //opsForSet 
        //opsForHash
        //opsForZSet
        //opsForGeo
        //opsForHyperLogLog
//        RedisConnection connection = redisTemplate.getConnectionFactory().getConnection();
//        connection.flushAll();
//        connection.flushDb();
        redisTemplate.opsForValue().set("mykey","kuangshen");
        redisTemplate.opsForValue().set("mykey1","狂神说");
        System.out.println(redisTemplate.opsForValue().get("myke1"));
    }
```



关于对象的保存

![image-20210128142805518](redis.assets/image-20210128142805518.png)

需要序列化

```
//在企业中，我们所有的pojo都需要序列化
public class User implements Serializable {
    private String name;
    private int age;

}
```







# redis.conf详解

启动的时候，就通过配置文件来启动

==行家有没有，出手就知道==

> 单位

![image-20210128143848271](redis.assets/image-20210128143848271.png)

1.配置文件unit单位不区分大小写

> 包含



![image-20210128203840141](redis.assets/image-20210128203840141.png)

> 网络

```bash
bind 127.0.0.1 #绑定的IP
protected-mode yes #保护模式
port 6379 #端口号

```

> 通用

```bash
daemonize yes #以守护进程的方式打开，默认是no，我们需要自己 开启为yes
pidfile /var/run/redis_6379.pid #如果以后台的方式运行·，我们就需要指定一个pid文件
#日志
# Specify the server verbosity level.
# This can be one of:
# debug (a lot of information, useful for development/testing)
# verbose (many rarely useful info, but not a mess like the debug level)
# notice (moderately verbose, what you want in production probably) 生产环境
# warning (only very important / critical messages are logged)
loglevel notice
logfile "" #生成的日志文件位置名
databases 16 #数据库数量
always-show-logo yes #是否总显示logo
 
```

> 快照

持久化，在规定的是加密内，执行了多少次操作，则会持久到文件` .rbd ` `. aof `中

redis是内存数据库，如果没有持久化，那么数据断电及失！

```bash
#如果900s内，如果至少有一个key发生了修改，我们进行持久化操作
save 900 1
#如果300s内，如果至少有10个key发生了修改，我们进行持久化操作
save 300 10
#如果60s内，如果至少有1000key发生了修改，我们进行持久化操作
save 60 10000
#我们之后会学习持久化，会定义这个测试

stop-writes-on-bgsave-error yes #持久化如果出错 是否还需要继续工作
rdbcompression yes #是否压缩rdb 文件 需要消耗一些内存
rdbchecksum yes #保存 rdb 文件的时候，进行错误的检查校验
dir ./  #文件保存的目录



```



> replication   复制 我们讲解 主从复制的时候再进行讲解



> Security 安全

可以在这里设置redis的密码，默认是没有密码的 命令行修改密码

```bash
127.0.0.1:6379> ping
PONG
127.0.0.1:6379> CONFIG GET requirepass #获取密码
1) "requirepass"
2) ""
127.0.0.1:6379> CONFIG GET requirepass "123456" 
(error) ERR Unknown subcommand or wrong number of arguments for 'GET'. Try CONFIG HELP.
127.0.0.1:6379> CONFIG seet requirepass "123456" #修改密码 
(error) ERR Unknown subcommand or wrong number of arguments for 'seet'. Try CONFIG HELP.
127.0.0.1:6379> CONFIG set requirepass "123456"
OK
127.0.0.1:6379> auth 123456 #权限 使用密码登录
OK
127.0.0.1:6379> CONFIG GET requirepass 
1) "requirepass"
2) "123456"

```

> 限制clients

```bash
# maxclients 10000 #设置连接上redis的最大客户端
# maxmemory <bytes> #redis配置最大的内存容量
# maxmemory-policy noeviction #内存到达上限之后的处理策略
 maxmemory-policy 六种方式
1、volatile-lru：只对设置了过期时间的key进行LRU（默认值） 

2、allkeys-lru ： 删除lru算法的key   

3、volatile-random：随机删除即将过期key   

4、allkeys-random：随机删除   

5、volatile-ttl ： 删除即将过期的   

6、noeviction ： 永不过期，返回错误

```

> append only模式 aof配置

```bash
appendonly no #默认是不开启aof模式的，默认是使用rdb持久化的，在大部分所有的情况下，rdb完全够用
appendfilename "appendonly.aof" # 持久化的文件的名字

# appendfsync always  #每次修改都会sync 消耗性能
appendfsync everysec  #每秒执行一次sync ，可能会丢失ls的数据

# appendfsync no #不执行sync，这个时候操作系统自己同步数据，速度更快


```

具体的配置，在持久化的时候详解！







# redis持久化

面试的时候 `持久化是重点`

redis是内存数据库，如果不能将内存中的数据库状态保存到磁盘中，那么一旦服务器进程退出，服务器中的数据库状态也会消失，所以redis提供了持久化功能

## RDB（redis Database）

> 什么是RDB

在主从复制中，rdb就是备用的了，从机上面

![image-20210128224839612](redis.assets/image-20210128224839612.png)

在指定的时间间隔内将内存中的数据集`快照`写入磁盘，也就是行话讲的Snapshot 快照，它恢复时将快照文件直接读到内存里。

redis会单独创建(fork)一个子进程来进行持久化，会将数据写入到一个临时文件中，待持久化过程结束了，再用这个临时文件替换上次持久化好的文件。整个文件中，主进程是不进行任何IO操作的。这就确保了极高的性能。如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那么RDB方式比AOF更加高效，RDB的缺点是最后一次持久化后的数据可能丢失。我们默认的是RDB，一般情况下部需要修改这个配置

有时候在生产环境我们会将这个文件进行备份！

`RDB保存的文件是dump.rdb` 都是在我们配置文件中快照中进行配置的

![image-20210129111242330](redis.assets/image-20210129111242330.png)

![image-20210129144439721](redis.assets/image-20210129144439721.png)

> 触发机制

1.save的规则满足的情况下，会自动触发rdb规则

2.执行save flushall 等命令也会触发我们的rdb规则 （kill是杀死进程 ）

3.退出redis ，也会产生rdb文件

备份就自动生成一个dump.rdb



![image-20210129145948033](redis.assets/image-20210129145948033.png)



>如果恢复rdb文件

1.只需要将rdb文件放在我们redis启动目录即可，redis启动的时候会自动检查 dump.rdb恢复其中的数据

2.查看需要存在的位置

```bash
127.0.0.1:6379> config get dir
1) "dir"
2) "/usr/local/bin" #如果在这个目录下存在dump.rdb文件，启动就会恢复其中的数据

```

> 几乎就他自己默认的配置就够用了，但是我们还是需要去学习

优点

1.适合大规模的数据恢复！

2.对数据的完整性要求不高

缺点：

1.需要一定的时间间隔 进程操作，如果redis意外挂机了，这个最后一次数据就没有了

2.fork进程的时候会占用一定的内存空间





## AOF(Append ONLY File)

将我们的所有命令都记录下来，history，恢复的时候就把这个文件全部在执行一遍！

> 是什么

![image-20210129151616517](redis.assets/image-20210129151616517.png)



以日志的形式来记录每个写操作，将redis执行过的所有指令记录下来（读操作不记录），只许追加文件但不可以改写文件，redis启动之初会读取该文件重新构建数据，换言之，redis重启的话就根据日志文件的内容将写指令 从前到后执行一次以完成数据的恢复工作

==Aof保存的是appendonly.aof文件==

>append

![image-20210129152104241](redis.assets/image-20210129152104241.png)

默认是不开启的，我们需要手动进行配置，我们只需要将appendonly 改为 yes 就开启了aof

重启，redis就可以生效了 就出现了

![image-20210129154517632](redis.assets/image-20210129154517632.png)

如果这个aof文件有问题（被错误修改了），这时候redis是启动不起来的，我们需要去修复这个aof文件

redis给我们提供了工具  `redis-check-aof  --fix`

![image-20210129165835954](redis.assets/image-20210129165835954.png)

如果文件正常，重启就可以直接使用了



> 重写

aof默认是文件的无限追加 ，文件会越来越大

![image-20210129170424156](redis.assets/image-20210129170424156.png)

如果aof文件大于64m，太大了，fork一个新的进程来将我们的文件进行重写



> 优点和缺点

```bash
appendonly no #默认是不开启aof模式的，默认是使用rdb持久化的，在大部分所有的情况下，rdb完全够用
appendfilename "appendonly.aof" # 持久化的文件的名字

# appendfsync always  #每次修改都会sync 消耗性能
appendfsync everysec  #每秒执行一次sync ，可能会丢失ls的数据

# appendfsync no #不执行sync，这个时候操作系统自己同步数据，速度更快
```

优点：

1.每一次修改都同步，文件的完澡会更好

2.没秒更新一次，可能会丢失一秒的数据

3.从不同步，效率最高的！

缺点：

1.相对于数据文件来说，aof远远大于rdb，修复的速度也比rdb慢

2.aof运行效率也比rdb慢，所以我们redis默认的配置就是rdb持久化

扩展：

> **RDB和AOP选择**

> **RDB 和 AOF 对比**

| RDB        | AOF    |              |
| ---------- | ------ | ------------ |
| 启动优先级 | 低     | 高           |
| 体积       | 小     | 大           |
| 恢复速度   | 快     | 慢           |
| 数据安全性 | 丢数据 | 根据策略决定 |

### 如何选择使用哪种持久化方式？

一般来说， 如果想达到足以媲美 PostgreSQL 的数据安全性， 你应该同时使用两种持久化功能。

如果你非常关心你的数据， 但仍然可以承受数分钟以内的数据丢失， 那么你可以只使用 RDB 持久化。

有很多用户都只使用 AOF 持久化， 但并不推荐这种方式： 因为定时生成 RDB 快照（snapshot）非常便于进行数据库备份， 并且 RDB 恢复数据集的速度也要比 AOF 恢复的速度要快。





# redis发布订阅

redis发布订阅是一种==消息通信模式==，发送者发送消息，订阅者接收消息。微信公众号，微博 关注系统！

redis客户端可以订阅任意数量的频道

订阅/发布消息图：

第一个：消息发送者，第二个：频道   第三个：消息订阅者

![image-20210129191127074](redis.assets/image-20210129191127074.png)



下图展示了频道 channel1 ， 以及订阅这个频道的三个客户端 —— client2 、 client5 和 client1 之间的关系：

![img](redis.assets/pubsub1.png)

当有新消息通过 PUBLISH 命令发送给频道 channel1 时， 这个消息就会被发送给订阅它的三个客户端：

![img](redis.assets/pubsub2.png)

> 发布订阅命令

| 序号 | 命令及描述                                                   |
| :--- | :----------------------------------------------------------- |
| 1    | [PSUBSCRIBE pattern [pattern ...\]](https://www.runoob.com/redis/pub-sub-psubscribe.html) 订阅一个或多个符合给定模式的频道。 |
| 2    | [PUBSUB subcommand [argument [argument ...\]]](https://www.runoob.com/redis/pub-sub-pubsub.html) 查看订阅与发布系统状态。 |
| 3    | [PUBLISH channel message](https://www.runoob.com/redis/pub-sub-publish.html) 将信息发送到指定的频道。 |
| 4    | [PUNSUBSCRIBE [pattern [pattern ...\]]](https://www.runoob.com/redis/pub-sub-punsubscribe.html) 退订所有给定模式的频道。 |
| 5    | [SUBSCRIBE channel [channel ...\]](https://www.runoob.com/redis/pub-sub-subscribe.html) 订阅给定的一个或多个频道的信息。 |
| 6    | [UNSUBSCRIBE [channel [channel ...\]]](https://www.runoob.com/redis/pub-sub-unsubscribe.html) 指退订给定的频道。 |



> 测试

发送端：

```bash
127.0.0.1:6379> publish kuangshen "hello,kuangshen" #发布者发布消息到频道
(integer) 1
127.0.0.1:6379> publish kuangshen "hello,redis"  #发布者发布消息到频道
(integer) 1

```

订阅方：

```bash
127.0.0.1:6379> SUBSCRIBE kuangshen #订阅一个频道 
Reading messages... (press Ctrl-C to quit)
1) "subscribe"
2) "kuangshen"
3) (integer) 1
#等待读取推送的信息
1) "message" #消息
2) "kuangshen" #那个频道的消息
3) "hello,kuangshen" #消息的具体内容
1) "message"
2) "kuangshen"
3) "hello,redis"

```

> 原理

每个 Redis 服务器进程都维持着一个表示服务器状态的 redis.h/redisServer 结构， 结构的 pubsub_channels 属性是一个字典， 这个字典就用于保存订阅频道的信息，其中，字典的键为正在被订阅的频道， 而字典的值则是一个链表， 链表中保存了所有订阅这个频道的客户端。

![在这里插入图片描述](redis.assets/2020051321554964.png)

客户端订阅，就被链接到对应频道的链表的尾部，退订则就是将客户端节点从链表中移除。 

c语言



> 缺点

1. 如果一个客户端订阅了频道，但自己读取消息的速度却不够快的话，那么不断积压的消息会使redis输出缓冲区的体积变得越来越大，这可能使得redis本身的速度变慢，甚至直接崩溃。
2. 这和数据传输可靠性有关，如果在订阅方断线，那么他将会丢失所有在短线期间发布者发布的消息。

> 应用

1. 消息订阅：公众号订阅，微博关注等等（起始更多是使用消息队列来进行实现）
2. 多人在线聊天室。

稍微复杂的场景，我们就会使用消息中间件MQ处理。



# redis主从复制 

> 概念 （主仆关系 ）

 主从复制，是指将一台Redis服务器的数据，复制到其他的Redis服务器。前者称为主节点（Master/Leader）,后者称为从节点（Slave/Follower）， 数据的复制是单向的！只能由主节点复制到从节点（主节点以写为主、从节点以读为主）。

默认情况下，每台Redis服务器都是主节点，一个主节点可以有0个或者多个从节点，但每个从节点只能由一个主节点。

> 作用

1. 数据冗余：主从复制实现了数据的热备份，是持久化之外的一种数据冗余的方式。
2. 故障恢复：当主节点故障时，从节点可以暂时替代主节点提供服务，是一种服务冗余的方式
3. 负载均衡：在主从复制的基础上，配合读写分离，由主节点进行写操作，从节点进行读操作，分担服务器的负载；尤其是在多读少写的场景下，通过多个从节点分担负载，提高并发量。
4. 高可用基石：主从复制还是哨兵和集群能够实施的基础。

> 为什么使用集群

1. 单台服务器难以负载大量的请求 （产生宕机）
2. 单台服务器故障率高，系统崩坏概率大
3. 单台服务器内存容量有限。 ==单台redsi最大使用内存不超过20g==

电商网站上的商品，一般都是一次传完，无数次浏览的，说专业也就是“多读少写”

对于这种场景，我们可以使用如下架构

![image-20210129201819683](redis.assets/image-20210129201819683.png)

主从复制，读写分离！80%的情况下都是在进行读操作！减缓服务器的压力！架构中经常使用！`一主二从`

只要在公司中，主从复制就是必须要使用的，因为在真实的项目中不可能单机使用redis

> 环境配置

我们在讲解配置文件的时候，注意到有一个`replication`模块

只配置从库，不用配置主库！

```bash
127.0.0.1:6379> info replication #查看当前库的信息
# Replication
role:master  #角色
connected_slaves:0 #没有从机
master_replid:61a350f163fa124cd9cf96763c9cc2fbd7c4a2d7
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:0
repl_backlog_size:1048576
repl_backlog_first_byte_offset:0
repl_backlog_histlen:0

```

修改了哪些东西

1.pid

2.port

3.dump.rdb

4.log

完成环境配置之后，可以启动3个redis服务器，可以通过进程信息查询

![image-20210130140752102](redis.assets/image-20210130140752102.png)

## 一主二从

==默认情况下，每台redis服务器都是主节点；==我们一般情况下只用配置从机就好了

认老大，一主(79)二从(80,81)

```bash
127.0.0.1:6380> SLAVEOF 127.0.0.1 6379 #SLAVEOF host port 找谁当自己的老大
OK
127.0.0.1:6380> info replication
# Replication
role:slave #当前角色是谁
master_host:127.0.0.1 #可以看到主机的信息
master_port:6379
master_link_status:up
master_last_io_seconds_ago:9
master_sync_in_progress:0
slave_repl_offset:0
slave_priority:100
slave_read_only:1
connected_slaves:0
master_replid:7ab6694e3b51e04323fe45bf28cc9502146a6846
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:0
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:0
#在主机中查看
127.0.0.1:6379> info replication
# Replication
role:master
connected_slaves:1 #多了个一个从机的配置
slave0:ip=127.0.0.1,port=6380,state=online,offset=14,lag=0  #多了个一个从机的配置
master_replid:7ab6694e3b51e04323fe45bf28cc9502146a6846
master_replid2:0000000000000000000000000000000000000000
master_repl_offset:14
second_repl_offset:-1
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1
repl_backlog_histlen:14

```

如果2个都配置完成了，就是有2个从机的

![image-20210130144214698](redis.assets/image-20210130144214698.png)

真实的主从配置应该是在配置文件中进行的，这样话是永久的，我们这里使用的是命令行，暂时性的

> 细节

主机只能写，从机不能写只能读,主机中的信息和数据，都会被从机给保存

主机写

![image-20210130150639235](redis.assets/image-20210130150639235.png)

从机只能读取内容

![image-20210130150727288](redis.assets/image-20210130150727288.png)

> 测试

当主机断电宕机后，默认情况下从机的角色不会发生变化 ，集群中只是失去了写操作，当主机恢复以后，又会连接上从机恢复原状。

当从机断电宕机后，若不是使用配置文件配置的从机，再次启动后作为主机是无法获取之前主机的数据的，若此时重新配置称为从机，又可以获取到主机的所有数据。这里就要提到一个同步原理。

> 同步复制原理

***全量同步**

Redis全量复制一般发生在Slave初始化阶段，这时Slave需要将Master上的所有数据都复制一份。

具体步骤如下：

- 从服务器连接主服务器，发送SYNC命令；
- 主服务器接收到SYNC命名后，开始执行BGSAVE命令生成RDB文件并使用缓冲区记录此后执行的所有写命令；
- 主服务器BGSAVE执行完后，向所有从服务器发送快照文件，并在发送期间继续记录被执行的写命令；
- 从服务器收到快照文件后丢弃所有旧数据，载入收到的快照；
- 主服务器快照发送完毕后开始向从服务器发送缓冲区中的写命令；
- 从服务器完成对快照的载入，开始接收命令请求，并执行来自主服务器缓冲区的写命令；

> 层层链路

上一个M（master） 连接下一个s（slave）

![image-20210130153650635](redis.assets/image-20210130153650635.png)

这时候也可以完成我们的主从复制

> 如果没有老大了，这个能不能选择一个老大出来？手动

谋朝篡位

如果主机断开了连接，我们可以使用` SLAVEOF no one `让自己变成主机！其他的节点就可以手动连接到最新的这个主节点（手动）上

```bash
127.0.0.1:6380> SLAVEOF no one
OK
127.0.0.1:6380> info replication
# Replication
role:master
connected_slaves:1

```



















## 哨兵模式（自动配置原理）

自动选择老大

哨兵模式是一种特殊的模式，首先Redis提供了哨兵的命令，哨兵是一个独立的进程，作为进程，它会独立运行。其原理是**哨兵通过发送命令，等待Redis服务器响应，从而监控运行的多个Redis实例。**

![img](redis.assets/11320039-57a77ca2757d0924.png)

这里的哨兵有两个作用

- 通过发送命令，让Redis服务器返回监控其运行状态，包括主服务器和从服务器。
- 当哨兵监测到master宕机，会自动将slave切换成master，然后通过**发布订阅模式**通知其他的从服务器，修改配置文件，让它们切换主机。

然而一个哨兵进程对Redis服务器进行监控，可能会出现问题，为此，我们可以使用多个哨兵进行监控。各个哨兵之间还会进行监控，这样就形成了多哨兵模式。

用文字描述一下**故障切换（failover）**的过程。假设主服务器宕机，哨兵1先检测到这个结果，系统并不会马上进行failover过程，仅仅是哨兵1主观的认为主服务器不可用，这个现象成为**主观下线**。当后面的哨兵也检测到主服务器不可用，并且数量达到一定值时，那么哨兵之间就会进行一次投票，投票的结果由一个哨兵发起，进行failover操作。切换成功后，就会通过发布订阅模式，让各个哨兵把自己监控的从服务器实现切换主机，这个过程称为**客观下线**。这样对于客户端而言，一切都是透明的。

> 测试

sentinel.conf

哨兵的核心配置 

```
sentinel monitor mymaster 127.0.0.1 6379 1
```

* 数字1表示 ：当一个哨兵主观认为主机断开，就可以客观认为主机故障，然后开始选举新的主机。

启动哨兵

```bash
redis-sentinel kconfig/sentinel.conf
```

成功启动哨兵模式

主机宕机  

如果Master节点断开了，这个时候就会从从机中随机选择一个服务器（这里有一个投票算法）

哨兵日志

```bash
14705:X 30 Jan 2021 16:00:35.494 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
14705:X 30 Jan 2021 16:00:35.498 # Sentinel ID is c24456f69e3171b7e1baf84d3a3ad7793af9a00e
14705:X 30 Jan 2021 16:00:35.498 # +monitor master mymaster 127.0.0.1 6379 quorum 1
14705:X 30 Jan 2021 16:00:35.499 * +slave slave 127.0.0.1:6380 127.0.0.1 6380 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:00:35.503 * +slave slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.142 # +sdown master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.142 # +odown master mymaster 127.0.0.1 6379 #quorum 1/1
14705:X 30 Jan 2021 16:02:24.142 # +new-epoch 1
14705:X 30 Jan 2021 16:02:24.142 # +try-failover master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.147 # +vote-for-leader c24456f69e3171b7e1baf84d3a3ad7793af9a00e 1
14705:X 30 Jan 2021 16:02:24.147 # +elected-leader master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.147 # +failover-state-select-slave master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.206 # +selected-slave slave 127.0.0.1:6380 127.0.0.1 6380 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.206 * +failover-state-send-slaveof-noone slave 127.0.0.1:6380 127.0.0.1 6380 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.282 * +failover-state-wait-promotion slave 127.0.0.1:6380 127.0.0.1 6380 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.980 # +promoted-slave slave 127.0.0.1:6380 127.0.0.1 6380 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:24.980 # +failover-state-reconf-slaves master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:25.066 * +slave-reconf-sent slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:26.007 * +slave-reconf-inprog slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:26.007 * +slave-reconf-done slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:26.064 # +failover-end master mymaster 127.0.0.1 6379
14705:X 30 Jan 2021 16:02:26.064 # +switch-master mymaster 127.0.0.1 6379 127.0.0.1 6380 #转移 选出6380当选主机
14705:X 30 Jan 2021 16:02:26.064 * +slave slave 127.0.0.1:6381 127.0.0.1 6381 @ mymaster 127.0.0.1 6380
14705:X 30 Jan 2021 16:02:26.064 * +slave slave 127.0.0.1:6379 127.0.0.1 6379 @ mymaster 127.0.0.1 6380
14705:X 30 Jan 2021 16:02:56.085 # +sdown slave 127.0.0.1:6379 127.0.0.1 6379 @ mymaster 127.0.0.1 6380

```



![image-20210130160501801](redis.assets/image-20210130160501801.png)

如果主机回来了，只能归并到新的主机下面，当做从机，这就是哨兵模式的规则！

> 哨兵模式

```bash
# Example sentinel.conf
 
# 哨兵sentinel实例运行的端口 默认26379
port 26379
 
# 哨兵sentinel的工作目录
dir /tmp
 
# 哨兵sentinel监控的redis主节点的 ip port 
# master-name  可以自己命名的主节点名字 只能由字母A-z、数字0-9 、这三个字符".-_"组成。
# quorum 当这些quorum个数sentinel哨兵认为master主节点失联 那么这时 客观上认为主节点失联了
# sentinel monitor <master-name> <ip> <redis-port> <quorum>
sentinel monitor mymaster 127.0.0.1 6379 1
 
# 当在Redis实例中开启了requirepass foobared 授权密码 这样所有连接Redis实例的客户端都要提供密码
# 设置哨兵sentinel 连接主从的密码 注意必须为主从设置一样的验证密码
# sentinel auth-pass <master-name> <password>
sentinel auth-pass mymaster MySUPER--secret-0123passw0rd
 
 
# 指定多少毫秒之后 主节点没有应答哨兵sentinel 此时 哨兵主观上认为主节点下线 默认30秒
# sentinel down-after-milliseconds <master-name> <milliseconds>
sentinel down-after-milliseconds mymaster 30000
 
# 这个配置项指定了在发生failover主备切换时最多可以有多少个slave同时对新的master进行 同步，
这个数字越小，完成failover所需的时间就越长，
但是如果这个数字越大，就意味着越 多的slave因为replication而不可用。
可以通过将这个值设为 1 来保证每次只有一个slave 处于不能处理命令请求的状态。
# sentinel parallel-syncs <master-name> <numslaves>
sentinel parallel-syncs mymaster 1
 
 
 
# 故障转移的超时时间 failover-timeout 可以用在以下这些方面： 
#1. 同一个sentinel对同一个master两次failover之间的间隔时间。
#2. 当一个slave从一个错误的master那里同步数据开始计算时间。直到slave被纠正为向正确的master那里同步数据时。
#3.当想要取消一个正在进行的failover所需要的时间。  
#4.当进行failover时，配置所有slaves指向新的master所需的最大时间。不过，即使过了这个超时，slaves依然会被正确配置为指向master，但是就不按parallel-syncs所配置的规则来了
# 默认三分钟
# sentinel failover-timeout <master-name> <milliseconds>
sentinel failover-timeout mymaster 180000
 
# SCRIPTS EXECUTION
 
#配置当某一事件发生时所需要执行的脚本，可以通过脚本来通知管理员，例如当系统运行不正常时发邮件通知相关人员。
#对于脚本的运行结果有以下规则：
#若脚本执行后返回1，那么该脚本稍后将会被再次执行，重复次数目前默认为10
#若脚本执行后返回2，或者比2更高的一个返回值，脚本将不会重复执行。
#如果脚本在执行过程中由于收到系统中断信号被终止了，则同返回值为1时的行为相同。
#一个脚本的最大执行时间为60s，如果超过这个时间，脚本将会被一个SIGKILL信号终止，之后重新执行。
 
#通知型脚本:当sentinel有任何警告级别的事件发生时（比如说redis实例的主观失效和客观失效等等），将会去调用这个脚本，
#这时这个脚本应该通过邮件，SMS等方式去通知系统管理员关于系统不正常运行的信息。调用该脚本时，将传给脚本两个参数，
#一个是事件的类型，
#一个是事件的描述。
#如果sentinel.conf配置文件中配置了这个脚本路径，那么必须保证这个脚本存在于这个路径，并且是可执行的，否则sentinel无法正常启动成功。
#通知脚本
# sentinel notification-script <master-name> <script-path>
  sentinel notification-script mymaster /var/redis/notify.sh
 
# 客户端重新配置主节点参数脚本
# 当一个master由于failover而发生改变时，这个脚本将会被调用，通知相关的客户端关于master地址已经发生改变的信息。
# 以下参数将会在调用脚本时传给脚本:
# <master-name> <role> <state> <from-ip> <from-port> <to-ip> <to-port>
# 目前<state>总是“failover”,
# <role>是“leader”或者“observer”中的一个。 
# 参数 from-ip, from-port, to-ip, to-port是用来和旧的master和新的master(即旧的slave)通信的
# 这个脚本应该是通用的，能被多次调用，不是针对性的。
# sentinel client-reconfig-script <master-name> <script-path>
sentinel client-reconfig-script mymaster /var/redis/reconfig.sh
```



# redis缓存穿透和雪崩



> 缓存穿透 (查不到导致的)

概念：

在默认情况下，用户请求数据时，会先在缓存(Redis)中查找，若没找到即缓存未命中，再在数据库中进行查找，数量少可能问题不大，可是一旦大量的请求数据（例如秒杀场景）缓存都没有命中的话，就会全部转移到数据库上，造成数据库极大的压力，就有可能导致数据库崩溃。网络安全中也有人恶意使用这种手段进行攻击被称为洪水攻击。



解决方案：

1. 布隆过滤器

对所有可能查询的参数以Hash的形式存储，以便快速确定是否存在这个值，在控制层先进行拦截校验，校验不通过直接打回，减轻了存储系统的压力。

![在这里插入图片描述](redis.assets/20200513215824722.jpg)





2. 缓存空对象

一次请求若在缓存和数据库中都没找到，就在缓存中方一个空对象用于处理后续这个请求。

![在这里插入图片描述](redis.assets/20200513215836317.jpg)



这样做有一个缺陷：存储空对象也需要空间，大量的空对象会耗费一定的空间，存储效率并不高。解决这个缺陷的方式就是设置较短过期时间

即使对空值设置了过期时间，还是会存在缓存层和存储层的数据会有一段时间窗口的不一致，这对于需要保持一致性的业务会有影响

> 缓存击穿(量太大，缓存过期)比如微博热搜 

概念：

相较于缓存穿透，缓存击穿的目的性更强，一个存在的key，在`缓存过期`的一刻，同时有大量的请求，这些请求都会击穿到DB，造成瞬时DB请求量大、压力骤增。这就是缓存被击穿，只是针对其中某个key的缓存不可用而导致击穿，但是其他的key依然可以使用缓存响应。

比如`热搜排行`上，一个热点新闻被同时大量访问就可能导致缓存击穿。

**解决方案**

1. 设置热点数据永不过期

   这样就不会出现热点数据过期的情况，但是当Redis内存空间满的时候也会清理部分数据，而且此种方案会占用空间，一旦热点数据多了起来，就会占用部分空间。

2. 加互斥锁(分布式锁)

   在访问key之前，采用SETNX（set if not exists）来设置另一个短期key来锁住当前key的访问，访问结束再删除该短期key。保证同时刻只有一个线程访问。这样对锁的要求就十分高。



> 缓存雪崩

概念

大量的key设置了相同的过期时间，导致在缓存在同一时刻全部失效，造成瞬时DB请求量大、压力骤增，引起雪崩。

![在这里插入图片描述](redis.assets/20200513215850428.jpeg)



**解决方案**

- redis高可用

  这个思想的含义是，既然redis有可能挂掉，那我多增设几台redis，这样一台挂掉之后其他的还可以继续工作，其实就是搭建的集群

- 限流降级

  这个解决方案的思想是，在缓存失效后，通过加锁或者队列来控制读数据库写缓存的线程数量。比如对某个key只允许一个线程查询数据和写缓存，其他线程等待。

- 数据预热

  数据加热的含义就是在正式部署之前，我先把可能的数据先预先访问一遍，这样部分可能大量访问的数据就会加载到缓存中。在即将发生大并发访问前手动触发加载缓存不同的key，设置不同的过期时间，让缓存失效的时间点尽量均匀。







