package com.kuang;

import redis.clients.jedis.Jedis;

public class TestPing {
    public static void main(String[] args) {
        //new 对象即可
        Jedis jedis=new Jedis("127.0.0.1",6379);
        //jedis 所有命令都是我们之前学的所有指令
        System.out.println(jedis.ping());
    }
}
